import axios from 'axios'
export default {
  getItems: function () {
    return axios.get(`photos/`)
  },
  getCategories: function () {
    return axios.get(`albums/`)
  },
  getFullCatalog: function () {
    //TODO: set promises to Promises.all
    let result = {
      category: [],
      items: []
    }
    return new Promise((resolve, reject) => {
      this.getCategories()
        .then((response) => {
          result.category = response.data.slice(Math.floor(Math.random() * 10 + Math.random()), Math.floor(Math.random() * 20 + 5))
          return this.getItems()
        })
        .then((response) => {
          result.items = response.data.filter((e) => e.albumId === result.category[0].id)
          resolve (result)
        })
        .catch((e) => {
          reject(e)
        })
    })
  }
}
