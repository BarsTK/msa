import Vue from 'vue'

Vue.mixin({
  head () {
    return {
      title: this.title ? this.title : 'Title',
      titleTemplate: '%s - Pfood',
      meta: [
        { hid: 'description', name: 'description', content: 'Meta description' }
      ]
    }
  },
  fetch ({ store, params }) {
    return new Promise(((resolve, reject) => {
      let promises = []
      promises.push(Promise.resolve(true))
      if (store.state.catalog.categoryList.length <= 0) {
        promises.push(new Promise(((resolve1, reject1) => {
          let categoryList = [
            {
              id: 1,
              name: 'Снижение веса',
              img : '/categories/losew8.svg',
              altImg: '/categories/losew8_alt.svg',
              code: 'sn_vesa',
              shortName: 'Снижение',
              children: [
                {
                  id: 10,
                  categoryId: 1,
                  code: 'sn_vesa_gold',
                  name: 'Gold'
                },
                {
                  id: 11,
                  categoryId: 1,
                  code: 'sn_vesa_silver',
                  name: 'Silver'
                }
              ]
            },
            {
              id: 2,
              name: 'Белковое питание',
              code: 'belka',
              shortName: 'Баланс',
              img : '/categories/prot_meal.svg',
            },
            {
              id: 3,
              name: 'Сбалансированное питание',
              shortName: 'Набор',
              code: 'nabor',
              img : '/categories/balance_meal.svg',
              altImg: '/categories/balance_meal_alt.svg'
            },
            {
              id: 4,
              name: 'Спортивное питание',
              img : '/categories/sport_meal.svg',
              altImg: '/categories/sport_meal_alt.svg',
              code: 'sport',
              shortName: 'Спорт',
            },
            {
              id: 5,
              name: 'Индивидуальное питание',
              img : '/categories/personal_meal.svg',
              altImg: '/categories/personal_meal_alt.svg',
              code: 'individ',
              shortName: 'Индивид',
            },
            {
              id: 6,
              name: 'Питание при заболеваниях',
              img : '/categories/health_meal.svg',
              altImg: '/categories/health_meal_alt.svg',
              code: 'bolezn',
              shortName: 'Забол',
              link: '/6',
            },
            {
              id: 7,
              name: 'Вегетарианское питание',
              img : '/categories/vegan_meal.svg',
              altImg: '/categories/vegan_meal_alt.svg',
              shortName: 'Вегетар',
              code: 'vegetarianskoe'
            },
            {
              id: 8,
              name: 'Постное питание',
              img : '/categories/postnoe_meal.svg',
              altImg: '/categories/postnoe_meal_alt.svg',
              code: 'postn',
              shortName: 'Постное',
            },
            {
              id: 9,
              name: 'Detox',
              shortName: 'Detox',
              code: 'detox',
              img : '/categories/detox.svg',
              altImg: '/categories/detox_alt.svg'
            }
          ]
          resolve1(categoryList)
        })))
      }
      if (store.state.catalog.items.length <= 0) {
        promises.push(new Promise(((resolve1, reject1) => {
          let items = []
          for (let i = 1; i <= 11; i++) {
            items.push({
              id: 1,
              categoryId: i + 1,
              code: 'belka/item1',
              name: `Item name ${i}`,
              minPrice: Math.floor(Math.random() * 1000) + 100,
              calories: Math.floor(Math.random() * 2000) + 100,
              img: '/slider/girl.png',
              period: 1,
              options: [
                {
                  name: 'Блюда',
                  value: 5
                },
                {
                  name: 'Белки',
                  value: Math.floor(Math.random() * 100) + 100
                },
                {
                  name: 'Жиры',
                  value: Math.floor(Math.random() * 30) + 30
                },
                {
                  name: 'Углеводы',
                  value: Math.floor(Math.random() * 80) + 80
                }
              ]
            })
            items.push({
              id: 2,
              categoryId: i + 1,
              period: 1,
              code: 'belka/item1',
              name: `Item name ${i}`,
              minPrice: Math.floor(Math.random() * 1000) + 100,
              calories: Math.floor(Math.random() * 2000) + 100,
              img: '/slider/girl.png',
              options: [
                {
                  name: 'Блюда',
                  value: 5
                },
                {
                  name: 'Белки',
                  value: Math.floor(Math.random() * 100) + 100
                },
                {
                  name: 'Жиры',
                  value: Math.floor(Math.random() * 30) + 30
                },
                {
                  name: 'Углеводы',
                  value: Math.floor(Math.random() * 80) + 80
                }
              ]
            })
            items.push({
              id: 3,
              categoryId: i + 1,
              period: 2,
              name: `Item name ${i}`,
              minPrice: Math.floor(Math.random() * 1000) + 100,
              calories: Math.floor(Math.random() * 2000) + 100,
              img: '/slider/girl.png',
              code: 'belka/item2',
              options: [
                {
                  name: 'Блюда',
                  value: 5
                },
                {
                  name: 'Белки',
                  value: Math.floor(Math.random() * 100) + 100
                },
                {
                  name: 'Жиры',
                  value: Math.floor(Math.random() * 30) + 30
                },
                {
                  name: 'Углеводы',
                  value: Math.floor(Math.random() * 80) + 80
                }
              ]
            })
            items.push({
              id: 4,
              categoryId: i + 1,
              period: 2,
              name: `Item name ${i}`,
              minPrice: Math.floor(Math.random() * 1000) + 100,
              calories: Math.floor(Math.random() * 2000) + 100,
              img: '/slider/girl.png',
              code: 'belka/item2',
              options: [
                {
                  name: 'Блюда',
                  value: 5
                },
                {
                  name: 'Белки',
                  value: Math.floor(Math.random() * 100) + 100
                },
                {
                  name: 'Жиры',
                  value: Math.floor(Math.random() * 30) + 30
                },
                {
                  name: 'Углеводы',
                  value: Math.floor(Math.random() * 80) + 80
                }
              ]
            })
          }
          resolve1(items)
        })))
      }
      Promise.all(promises)
        .then(([resolver, categories, items]) => {
          if (categories && categories.length > 0) store.dispatch('catalog/setCategoryListAction', categories)
          if (items && items.length > 0) store.dispatch('catalog/setItemsAction', items)
          resolve(true)
        })
    }))
  },
  asyncData ({params, error}) {
    // TODO: use it if seo not found
    // error({ statusCode: 404, message: 'Post not found' })
  }
})
