export const state = () => ({
  dishesList: [],
})

export const getters = {
  getDishes: (state) => { return state.dishesList}
}

export const actions = {
  setDishesAction ({commit}, data) {
    commit('setDishesMutation', data)
  }
}

export const mutations = {
  setDishesMutation (state, data) {
    state.dishesList = data
  }
}
