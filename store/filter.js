export const state = () => ({
  filterList: {},
})

export const getters = {
  getFilterList: (state) => { return state.filterList}
}

export const actions = {
  setFilterListAction ({commit}, data) {
    commit('setFilterListMutation', data)
  },
  setFilterListItemAction ({commit}, data) {
    commit('setFilterListItemMutation', data)
  },
  deleteListAction ({commit}) {
    commit('deleteListMutation')
  },
  deleteItemAction ({commit}, data) {
    commit('deleteItemMutation', data)
  }
}

export const mutations = {
  setFilterListMutation (state, data) {
    state.filterList = data
  },
  setFilterListItemMutation (state, data) {
    state.filterList[data.key] = data.value
  },
  deleteListMutation (state) {
    state.filterList = {}
  },
  deleteItemMutation (state, data) {
    delete state.filterList[data.key]
  }
}
