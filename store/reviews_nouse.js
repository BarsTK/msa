export const state = () => ({
  reviewsList: []
});

export const getters = {
  getReviews: state => state.reviewsList,
  getReviewById: state => id => {
    return state.reviewsList.find(review => review.id === id);
  }
};

export const mutations = {
  setReviewsMutation (state, data) {
    state.reviewsList = data
  },
  addReviewItemMutation (state, item) {
    state.reviewsList.push(item)
  }
};

export const actions = {
  setReviews ({commit}, data) {
    commit('setReviewsMutation', data)
  },
  addItemReview ({commit}, item) {
    commit('addReviewItemMutation', item)
  }
};
