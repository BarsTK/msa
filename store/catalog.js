export const state = () => ({
  categoryList: [],
  items: []
})

export const getters = {
  getCategories: (state) => { return state.categoryList },
  getItems: (state) => { return state.items },
  getItemsForCategory: state => id => { return state.items.filter((e) => e.categoryId === id) },
  getCategoryByCode: state => code => {
    let arr = state.categoryList.map((e) => e)
    let ch = []
    arr.forEach((e) => {
      if (e.children && e.children.length > 0) {
        e.children.forEach((i) => ch.push(i))
      }
    })
    ch.forEach((e) => {
      arr.push(e)
    })
    return arr.filter((e) => e.code === code).shift()
  },
  getFirstCategory: state => { return state.categoryList[0] }
}

export const actions = {
  setCategoryListAction ({commit}, data) {
    commit('setCategoryListMutation', data)
  },
  setItemsAction ({commit}, data) {
    commit('setItemsMutation', data)
  }
}

export const mutations = {
  setCategoryListMutation (state, data) {
    state.categoryList = data
  },
  setItemsMutation (state, data) {
    state.items = data
  }
}
