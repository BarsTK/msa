export default function ({ store, redirect }) {
  if (store.state.catalog.categoryList.length > 0) {
    return redirect('/catalog/' + store.state.catalog.categoryList[0].id)
  }
}
